中文編碼
目的

這個單元，我們將介紹如何在Python中使用中文
說明

在之後的練習中，python script中常會加入中文註解，為了讓電腦可以知道這件事情，我們在前面會加上中文編號的代號，下面的例子說明電腦現在認識utf-8 編碼還有Big5 編碼了。

首先，讓我們回顧一下之前介紹過的例子
程式碼

total = 1 + 1
print " 1 + 1 =", total

執行結果

 1 + 1 = 2

現在讓我們在上面例子中的第二行加入中文
程式碼

total = 1+1
print " 一加一等於", total #總和

執行結果

File "second.py", line 2
SyntaxError: Non-ASCII character '\xe4' in file second.py on line 2, but no encoding declared; 
see http://www.python.org/peps/pep-0263.html for details

你會看見像上面的錯誤訊息，這表示電腦並沒有辦法正確辨認出加入的中文字。如果需要讓電腦知道我們將會使用中文字，我們可以加入中文編碼的資訊。第一行表示使用 utf-8 編碼，第二行表示使用 Big5 編碼。

#-*- coding: utf-8 -*-　　 
#-*- coding: cp950 -*-　
total = 1 + 1
print " 一加一等於", total #總和

執行結果

 一加一等於 2

如此一來，可以電腦就能夠處理含有中文字的程式碼。
本單元結論

    中文編碼需預先告知

